


<!DOCTYPE html>
<html lang="it">
  <head>
    <title> 80voglia di piada</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css"  href="stileHomePage.css">
    <?php
    session_start();
    if(isset($_SESSION["utente"])){

    ?>
    <style>
      #loggedDiv{
        visibility: visible;
      }
      #normalDiv{
        visibility: hidden;
      }
    </style>
<?php
    }

     ?>
  </head>
  <body>
<section id="sfondo">
    <header>
      <div id="normalDiv">
        <figure>
            <img  src="immagini/lucchetto.png" alt="lucchetto">
        </figure>
        <a href="login.html" id="login"> Login </a>
      </div>
      <div id="loggedDiv">
      <a href="ordinaProdotto.php" id="ordina"> Effettua un ordine </a>
      <a href="carrello.php" id="carrello"> Carrello </a>
      <label id="user"> <?php echo $_SESSION["utente"] ?> </label>
      <a href="logout.php" id="logout"> Logout </a>
      </div>
      <figure>
          <img id ="logo" src="immagini/logo.png" alt="logo azienda">
      </figure>

    </header>
  <nav>
    <ul >
      <a href="homePage.php" id="home">Chi siamo </a>
       <a href="prodotti.php">Prodotti</a>
      <a href="contatti.php">Contattaci </a>
    </ul>
  </nav>
    <section id="storia">
      <h1> Chi siamo </h1>
      <p>La nostra storia: </p>
      <p> Nati nel 2013, da oltre 4 anni siamo una società a conduzione familiare che si pone l'obiettivo di soddisfare tutti i clienti,
        offrendo loro i migliori prodotti e i migliori servizi possibili.</p>
      <p> Alcuni dei nostri servizi:</p>
      <ul>
        <li>Preparazione di ordini su ordinazione e possibilità di consegna a domicilio nella zona di Cesena.</li>
        <br/>
        <li>Realizzazione di buffet e aperitivi per lauree, eventi universitari e open day.</li>

      </ul>
    </section>
  <br/>
    <footer>
      <p id="footerPar"> 80voglia di piada s.r.l - Copyright 2018 - Email: <a id="mailLink" href="mailto:80vogliadipiada80@gmail.com">
 80vogliadipiada80@gmail.com</a></p>

    </footer>
</section>
  </body>
















</html>
