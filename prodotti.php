<?php
//Dichiarazione variabili per server
$servername="localhost";
$username ="root";
$password ="";
$database = "progetto";
$conn = new mysqli($servername,$username,$password,$database);
$query = "SELECT * FROM prodotto";
if($conn->connect_error){
  die("conn failed");
}
$res = $conn->query($query);
?>


<!DOCTYPE html>
<html lang="it">
  <head>
    <title> 80voglia di piada</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css"  href="stileHomePage.css">
    <?php
    session_start();
    if(isset($_SESSION["utente"])){

    ?>
    <style>
      #loggedDiv{
        visibility: visible;
      }
      #normalDiv{
        visibility: hidden;
      }
    </style>
<?php
    }

     ?>
  </head>
  <body>
<section>
    <header>
      <div id="normalDiv">
        <figure>
            <img src="immagini/lucchetto.png" alt="lucchetto">
        </figure>
        <a href="login.html" id="login"> Login </a>
      </div>
      <div id="loggedDiv">
          <a href="ordinaProdotto.php" id="ordina"> Effettua un ordine </a>
          <a href="carrello.php" id="carrello"> Carrello </a>
          <label id="user"> <?php echo $_SESSION["utente"] ?> </label>
          <a href="logout.php" id="logout"> Logout </a>
      </div>
      <figure>
          <img id="logo" src="immagini/logo.png" alt="logo azienda">
      </figure>

    </header>
  <nav>
    <ul >
      <a href="homePage.php" id="home">Chi siamo </a>
       <a href="prodotti.php">Prodotti</a>
      <a href="contatti.php">Contattaci </a>
    </ul>
  </nav>
    <section id="listaP">
      <h1> Forniamo una vasta scelta di prodotti</h1>

      <table class="table" border="1" width="500px">
			  <thead>
				<tr>
				  <th scope="col">Categoria</th>
				  <th scope="col">Nome</th>
				  <th scope="col">Prezzo</th>
				</tr>
			  </thead>
			  <tbody>
          <?php
					if($res->num_rows > 0){

						while($row=$res->fetch_assoc()){
							 ?>
							 <tr>
								 <td>
									 <?php
									 	echo $row["Categoria"]. " " ?>
									</td>
									<td>
 									 <?php
 									 	echo $row["Nome"]. "   " ?>
 									</td>
									<td>
 									 <?php
 									 	echo $row["Prezzo"]. "€"
                    ?>
 									</td>

								</tr>
								<?php

						}
					}
?>


        </tbody>
      </table>
    <p> <u>Per gli ordini che richiedono la consegna a domicilio viene applicato un sovrapprezzo di €0.70. </u></p>
    </section>
    <footer>
      <p id="footerPar"> 80voglia di piada s.r.l - Copyright 2018 - Email: <a id="mailLink" href="mailto:80vogliadipiada80@gmail.com">
80vogliadipiada80@gmail.com</a></p>

    </footer>
</section>
  </body>


</html>
