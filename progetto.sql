-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 27, 2018 alle 11:16
-- Versione del server: 10.1.28-MariaDB
-- Versione PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `progetto`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `categoria_prodotto`
--

CREATE TABLE `categoria_prodotto` (
  `Nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `categoria_prodotto`
--

INSERT INTO `categoria_prodotto` (`Nome`) VALUES
('Bruschetta'),
('Crescione'),
('Hamburger'),
('Piadina'),
('Pizza'),
('Rotolo');

-- --------------------------------------------------------

--
-- Struttura della tabella `categoria_utenti`
--

CREATE TABLE `categoria_utenti` (
  `Privilegio_Accesso` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `categoria_utenti`
--

INSERT INTO `categoria_utenti` (`Privilegio_Accesso`) VALUES
('Amministratore'),
('Utente');

-- --------------------------------------------------------

--
-- Struttura della tabella `metodo_pagamento`
--

CREATE TABLE `metodo_pagamento` (
  `Username` varchar(15) NOT NULL,
  `Numero` varchar(16) NOT NULL,
  `Codice` int(3) NOT NULL,
  `Mese` int(2) NOT NULL,
  `Anno` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `metodo_pagamento`
--

INSERT INTO `metodo_pagamento` (`Username`, `Numero`, `Codice`, `Mese`, `Anno`) VALUES
('poppinMike', '1254145236987412', 154, 12, 2018),
('lb12', '5326485212647890', 975, 10, 2021);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE `ordini` (
  `Codice_ordine` int(11) NOT NULL,
  `Username` varchar(15) NOT NULL,
  `Data` date NOT NULL,
  `Ora` time NOT NULL,
  `Indirizzo` varchar(50) NOT NULL,
  `PrezzoTotale` float NOT NULL,
  `Domicilio` tinyint(1) NOT NULL,
  `Completato` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ordini`
--

INSERT INTO `ordini` (`Codice_ordine`, `Username`, `Data`, `Ora`, `Indirizzo`, `PrezzoTotale`, `Domicilio`, `Completato`) VALUES
(2, 'lucan96', '2018-01-18', '11:00:00', 'Via Roncolo 1320 Longiano', 4, 1, 0),
(3, 'lucan96', '2018-01-18', '12:30:00', 'Via Giovanni Pascoli 13', 12.7, 1, 0),
(4, 'lb12', '2018-01-18', '13:25:00', '', 0.8, 0, 0),
(5, 'lb12', '2018-01-19', '19:00:00', 'Via Pistelli 9', 8.9, 1, 0),
(6, 'lb12', '2018-01-18', '17:00:00', '', 9.9, 1, 0),
(7, 'lb12', '2018-01-19', '15:30:00', '', 3.5, 1, 0),
(8, 'lb12', '2018-01-20', '20:35:00', '', 4.1, 1, 0),
(9, 'poppinMike', '2018-01-18', '17:55:00', 'Via Pistelli 9 Gambettola', 8.7, 1, 0),
(10, 'lb12', '2018-01-23', '12:00:00', '', 9.7, 1, 0),
(11, 'lb12', '2018-01-25', '12:00:00', 'Provo', 15.9, 1, 0),
(12, 'lb12', '2018-01-23', '11:00:00', 'Via del Lavoro 8', 7.6, 1, 1),
(13, 'lb12', '2018-01-23', '14:00:00', 'Prova', 3.9, 1, 0),
(14, 'lb12', '2018-01-24', '15:00:00', 'Prova', 13.7, 1, 1),
(15, 'lb12', '2018-01-24', '16:00:00', '', 9, 0, 0),
(16, 'poppinMike', '2018-01-25', '11:00:00', 'Via Pistelli 9 Cesena', 3.2, 1, 1),
(17, 'poppinMike', '2018-01-25', '13:00:00', '', 18, 0, 0),
(18, 'lb12', '2018-01-25', '11:00:00', '', 9, 0, 0),
(19, 'lb12', '2018-01-25', '17:46:00', '', 12, 0, 0),
(20, 'poppinMike', '2018-01-25', '18:51:00', 'Via Pistelli 9 Cesena', 9.7, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini_prodotti`
--

CREATE TABLE `ordini_prodotti` (
  `Numero_ordine` int(11) NOT NULL,
  `Codice_prodotto` int(11) NOT NULL,
  `Quantita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ordini_prodotti`
--

INSERT INTO `ordini_prodotti` (`Numero_ordine`, `Codice_prodotto`, `Quantita`) VALUES
(2, 1, 5),
(3, 17, 3),
(4, 1, 1),
(5, 23, 2),
(6, 9, 2),
(6, 26, 1),
(7, 11, 1),
(8, 23, 1),
(9, 17, 2),
(10, 31, 1),
(11, 30, 2),
(12, 30, 1),
(13, 1, 4),
(14, 29, 2),
(15, 31, 1),
(16, 9, 1),
(17, 31, 2),
(18, 31, 1),
(19, 37, 2),
(20, 31, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto`
--

CREATE TABLE `prodotto` (
  `ID` int(11) NOT NULL,
  `Nome` varchar(255) NOT NULL,
  `Prezzo` float NOT NULL,
  `Categoria` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `prodotto`
--

INSERT INTO `prodotto` (`ID`, `Nome`, `Prezzo`, `Categoria`) VALUES
(4, 'Piadina con prosciutto cotto', 2.1, 'Piadina'),
(6, 'Piadina con salame', 2.1, 'Piadina'),
(8, 'Piadina con speck', 2.1, 'Piadina'),
(9, 'Piadina con bresaola e grana', 2.5, 'Piadina'),
(11, 'P.crudo e rucola', 3.5, 'Rotolo'),
(12, 'P.crudo e insalata', 3.5, 'Rotolo'),
(13, 'P.cotto e funghi', 3.6, 'Rotolo'),
(14, 'Speck, radicchio rosso e scamorza', 3.8, 'Rotolo'),
(15, 'Speck, funghi e fontina', 3.7, 'Rotolo'),
(16, 'Tonno, cipolla e maionese', 3.8, 'Rotolo'),
(17, 'Bresaola, pinoli, grana e olio tartufato', 4, 'Rotolo'),
(18, 'Wurstel e gorgonzola', 3.7, 'Rotolo'),
(19, 'Formaggio, rucola e noci', 3.6, 'Rotolo'),
(20, 'Pomodoro e mozzarella', 4, 'Crescione'),
(21, 'Pomodoro, mozzarella, p. cotto', 4.5, 'Crescione'),
(22, 'Pomodoro, mozzarella, speck e funghi', 4.7, 'Crescione'),
(23, 'Patate e salsiccia', 4.1, 'Crescione'),
(24, 'Zucca, patate e scamorza', 4.1, 'Crescione'),
(25, 'Radicchio rosso, bresaola, gorgonzola', 4.3, 'Crescione'),
(26, '4 formaggi', 4.2, 'Crescione'),
(27, 'Pomodoro, mozzarella, tonno e cipolla', 4, 'Crescione'),
(28, 'Nutella', 3.9, 'Crescione'),
(29, 'Hamburger con pollo alla griglia', 6.5, 'Hamburger'),
(30, 'Manzo, cipolla, lattuga, maionese', 7.6, 'Hamburger'),
(31, 'P. crudo, grana e rucola', 9, 'Pizza'),
(33, 'Speck e fontina', 5, 'Rotolo'),
(34, 'Piadina con prosciutto crudo', 2.2, 'Piadina'),
(35, 'Piadina vuota', 0.8, 'Piadina'),
(36, 'Piadina con porchetta', 3, 'Piadina'),
(37, 'Aglio, olio, pomodoro', 6, 'Bruschetta');

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `Username` varchar(15) NOT NULL,
  `Password` varchar(30) NOT NULL,
  `Nome` varchar(50) NOT NULL,
  `Cognome` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Privilegio` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`Username`, `Password`, `Nome`, `Cognome`, `Email`, `Privilegio`) VALUES
('charisty', 'lucia08', 'Lucia', 'Foschini', 'luciafoschini8@gmail.com', 'Utente'),
('lb12', 'lb12', 'Luca', 'Bianchi', 'lucabianchi@hotmail.it', 'Utente'),
('lucan96', 'lu19ca96', 'Luca', 'Neri', 'lucaneri892@gmail.com', 'Amministratore'),
('nm59', 'n1e9r5i9', 'Marco', 'Neri', 'luca.n96@hotmail.it', 'Utente'),
('poppinMike', 'poppinmike1996', 'Michael', 'Manuzzi', 'luca.n96@hotmail.it', 'Utente');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `categoria_prodotto`
--
ALTER TABLE `categoria_prodotto`
  ADD PRIMARY KEY (`Nome`);

--
-- Indici per le tabelle `categoria_utenti`
--
ALTER TABLE `categoria_utenti`
  ADD PRIMARY KEY (`Privilegio_Accesso`);

--
-- Indici per le tabelle `metodo_pagamento`
--
ALTER TABLE `metodo_pagamento`
  ADD PRIMARY KEY (`Numero`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- Indici per le tabelle `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`Codice_ordine`),
  ADD KEY `Username` (`Username`);

--
-- Indici per le tabelle `ordini_prodotti`
--
ALTER TABLE `ordini_prodotti`
  ADD PRIMARY KEY (`Numero_ordine`,`Codice_prodotto`);

--
-- Indici per le tabelle `prodotto`
--
ALTER TABLE `prodotto`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Categoria` (`Categoria`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`Username`),
  ADD KEY `Privilegio` (`Privilegio`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `ordini`
--
ALTER TABLE `ordini`
  MODIFY `Codice_ordine` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `metodo_pagamento`
--
ALTER TABLE `metodo_pagamento`
  ADD CONSTRAINT `username-carta` FOREIGN KEY (`Username`) REFERENCES `utenti` (`Username`);

--
-- Limiti per la tabella `ordini`
--
ALTER TABLE `ordini`
  ADD CONSTRAINT `ordini_ibfk_1` FOREIGN KEY (`Username`) REFERENCES `utenti` (`Username`);

--
-- Limiti per la tabella `ordini_prodotti`
--
ALTER TABLE `ordini_prodotti`
  ADD CONSTRAINT `ordini_prodotti_ibfk_1` FOREIGN KEY (`Numero_ordine`) REFERENCES `ordini` (`Codice_ordine`);

--
-- Limiti per la tabella `prodotto`
--
ALTER TABLE `prodotto`
  ADD CONSTRAINT `prodotto_ibfk_1` FOREIGN KEY (`Categoria`) REFERENCES `categoria_prodotto` (`Nome`) ON DELETE CASCADE;

--
-- Limiti per la tabella `utenti`
--
ALTER TABLE `utenti`
  ADD CONSTRAINT `utenti_ibfk_1` FOREIGN KEY (`Privilegio`) REFERENCES `categoria_utenti` (`Privilegio_Accesso`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
