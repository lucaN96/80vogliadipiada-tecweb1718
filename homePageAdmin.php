<?php
session_start();
$servername="localhost";
$username ="root";
$password ="";
$database = "progetto";
$conn = new mysqli($servername,$username,$password,$database);
if(isset($_GET["valore"])){
  $codiceOrdine = $_GET["valore"];
  $_SESSION["codiceOrdineDaCompletare"] = $codiceOrdine;
  $cat = "SELECT * FROM ordini WHERE Codice_ordine='".$codiceOrdine."'";
  $res = $conn->query($cat);
  $row=$res->fetch_assoc();

  $username= $row["Username"];
  $data= $row["Data"];
  $ora= $row["Ora"];
  $indirizzo = $row["Indirizzo"];
  $prezzo= $row["PrezzoTotale"];
  $domicilio =$row["Domicilio"];

  $emailQuery = "SELECT Email FROM utenti WHERE Username='".$username."'";
  $ritorno = $conn->query($emailQuery);
  $temp = $ritorno->fetch_assoc();
  $emailUtente = $temp["Email"];
  $_SESSION["dom"] = $domicilio;
  $_SESSION["mail"] = $emailUtente;
}
 ?>


<!DOCTYPE html>
<html lang="it">
  <head>
    <title> Admin</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css"  href="stileAdmin.css">
  </head>
  <body >
<section >
    <header>

      <div id="loggedDiv">
          <a id="modifica" href="tariffario.php"> Modifica Tariffario </a>
        <figure>
            <img src="immagini/utente.png" alt="lucchetto" id="userLogo">
        </figure>

      <label id="user"> <?php echo $_SESSION["utente"] ?> </label>
      <a href="logout.php" id="logout"> Logout </a>

      </div>
      <figure>
          <img id="logo" src="immagini/logo.png" alt="logo azienda">
      </figure>

    </header>



    <form action="confermaOrdine.php" method="post">
    <!--  <fieldset>

        <legend> Gestione Ordini </legend>-->
        <br/>
        <label for="ordine" class="admin"> Scelta numero ordine </label><br/>
          <select id="ordine" class="admin">
            <?php
              if(!isset($_GET["valore"])){
             ?>
            <option> Scegli </option>
            <?php
            $today = date("Y-m-d");
            $query = "SELECT Codice_ordine FROM ordini WHERE Data='$today' AND Completato=0 ORDER BY Ora ASC";
            $res = $conn->query($query);

            while($row=$res->fetch_assoc()){
              ?>
              <option value="<?php echo $row["Codice_ordine"] ?>"> <?php echo $row["Codice_ordine"] ?> </option>

              <?php
            }
          }else{

            ?>
            <option> <?php echo $_GET["valore"] ?> </option>
           <?php
           $today = date("Y-m-d");
           $codice = $_GET["valore"];
           $query = "SELECT Codice_ordine FROM ordini WHERE Data='$today' AND Completato=0 AND Codice_ordine NOT LIKE '$codice' ORDER BY Ora ASC";
           $res = $conn->query($query);

           while($row=$res->fetch_assoc()){
             ?>
             <option value="<?php echo $row["Codice_ordine"] ?>"> <?php echo $row["Codice_ordine"] ?> </option>

             <?php
           }
          }
            ?>
          </select><br/><br/>

          <label for="riepilogo" class="admin"> Riepilogo ordine </label><br/>
          <table class="table" id="riepilogo" border="1" >
            <thead>
            <tr>
              <th id="nom">Nome</th>
              <th id="data">Data</th>
              <th id="o">Ora</th>
              <th id="c"> Indir. </th>
              <!--<th id="d"> Invio Fattorino </th>-->
              <th id="prezzo"> Costo </th>
            </tr>
            </thead>
            <tbody>
                   <tr>

                       <?php
                       if(isset($_GET["valore"])){
                         ?><td>
                           <?php
                        echo $username ?>
                      </td>
                      <td>
                       <?php
                        echo $data;

                         ?>
                      </td>
                      <td >
                       <?php
                       $orario = substr($ora,0,5);
                        echo $orario;
                        ?>
                      </td>
                      <td >
                        <?php
                        if($indirizzo == ""){
                          echo "Ritiro in negozio";
                        }else{
                         echo $indirizzo;
                       }
                         ?>
                      </td>
                    <!--  <td>
                        <?php
                        if($domicilio == 1){
                         echo "Sì";
                         }
                         else{
                           echo "No";
                         }
                         ?>
                      </td>-->
                      <td>
                        <?php
                         echo $prezzo." €";
                         ?>
                      </td>
                    </tr>
                    <?php


}

     ?>


            </tbody>
          </table>
          <br/>
          <?php
          if(isset($_GET["valore"])){
            $orderQuery = "SELECT Codice_prodotto,Quantita FROM ordini_prodotti WHERE Numero_ordine='".$codiceOrdine."'";
            $result = $conn->query($orderQuery);
            ?>
            <label for="dettagli" class="admin"> Dettagli ordine </label><br/>
            <table class="table" id="dettagli" border="1" width="80%">
              <thead>
              <tr>
                <th id="n">Categoria</th>
                <th id="p">Prodotto</th>
                <th id="q">Quantità</th>
              </tr>
              </thead>
              <tbody>
                <?php
                  while($row = $result->fetch_assoc()){
                    $product = "SELECT Nome,Categoria FROM prodotto WHERE ID='".$row["Codice_prodotto"]."'";
                    $t = $conn->query($product);
                    $risultato = $t->fetch_assoc();
                 ?>
                     <tr>
                       <td>
                         <?php

                          echo $risultato["Categoria"] ?>
                        </td>
                        <td>
                         <?php
                          echo $risultato["Nome"];

                           ?>
                        </td>
                        <td>
                         <?php
                          echo $row["Quantita"];
                          ?>
                        </td>
                      </tr>


            <?php
            }
          }
           ?>
         </tbody>
       </table>
       <br/>
          <input type="button" id="pronto" value="Ordine pronto" onclick="prodottoPronto()"/>

<!--      </fieldset>-->
    <br/><br/>

      </form>
      <script>
      var a= document.getElementById("dettagli");
      var b = document.getElementById("ordine");
        function prodottoPronto(){
          var x = b.options[b.selectedIndex].text;
          if(x=="Scegli"){
            alert("nessun ordine selezionato");
          }else{
            window.location.href = "processaOrdinePronto.php";
          }
        }

      </script>
    <footer>
      <p id="footerPar"> 80voglia di piada s.r.l - Copyright 2018 - Email: <a id="mailLink" href="mailto:80vogliadipiada80@gmail.com">
 80vogliadipiada80@gmail.com</a></p>

    </footer>
<script>

    var ordine = document.getElementById("ordine");
    function ottieniOrdine(){





            window.location.href="homePageAdmin.php?valore="+ordine.options[ordine.selectedIndex].text;

    }
    ordine.addEventListener('change',ottieniOrdine, false);

    </script>
</section>
  </body>
</html>
