<?php

session_start();
$costoTotale =0.0;
$tempCost=0.0;
if(isset($_SESSION["cart"])){
 $ser = serialize($_SESSION["cart"]);
 setcookie($_SESSION["utente"],$ser,time()+3600);
 $unser = unserialize($ser);
}
if(isset($_GET["elimina"])){
  $prodotto = $_GET["prodotto"];

  //$a = array_key_exists($prodotto,$_SESSION["cart"]);
  $indexCompleted = array_search($prodotto, $_SESSION["cart"]);
  echo $indexCompleted;
  unset($_SESSION["cart"][$indexCompleted]);
  foreach ($_SESSION["cart"] as $key  => $value){
    if ($key == $prodotto && $value == 1){
        unset($_SESSION["cart"][$key]);
    }else if($key == $prodotto && $value > 1){
        $_SESSION["cart"][$key] = $value -1;
    }
}
}
 $servername="localhost";
 $username ="root";
 $password ="";
 $database = "progetto";
 $conn = new mysqli($servername,$username,$password,$database);
 ?>



 <!DOCTYPE html>
 <html lang="it">
   <head>
     <title> Carrello Utente</title>

     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" type="text/css"  href="stileCarrello.css">
     <script src="jquery-3.2.1.min.js"></script>
     <style>
      #quantità,#el{
        text-align: center;
      }
      #elimina{
        color:blue;
      }

     </style>
   </head>
   <body>
<section id="mainSection">
     <header>

       <div id="loggedDiv">
       <a href="ordinaProdotto.php" id="ordina"> Effettua un ordine </a>
       <label id="user"> <?php echo $_SESSION["utente"] ?> </label>
       <a href="logout.php" id="logout"> Logout </a>
       </div>


     </header>
     <table class="table" id="tabella" border="1" width="80%">
       <thead>
       <tr>
         <th id="c">Categoria</th>
         <th id="n">Nome</th>
         <th id="q">Quantità</th>
         <th id="e"> Elimina </th>
       </tr>
       </thead>
       <tbody>
         <?php
         if(isset($_SESSION["cart"])){
         foreach($_SESSION["cart"] as $key  => $value){


              ?>
              <tr>
                <td headers="c">
                  <?php
                  $cat = "SELECT Categoria,Prezzo FROM prodotto WHERE Nome='".$key."'";
                  $res = $conn->query($cat);
                  $row=$res->fetch_assoc();
                  $tempCost= $row["Prezzo"];
                   echo $row["Categoria"]; ?>
                 </td>
                 <td headers="n">
                  <?php
                   echo $key;

                    ?>
                 </td>
                 <td header="q" id="quantità">
                  <?php
                   echo $value;
                   $tempCost = $tempCost*$value;
                   $costoTotale=$costoTotale+$tempCost;
                   ?>
                 </td>
                 <td id="el" headers="e">
                  <!--<a id="elimina" href="elimina.php"> Elimina prodotto </a>-->
                  <input type="button"  class="btnSelect" value="Elimina"/>
                 </td>

               </tr>
               <?php

           }
         }

?>


       </tbody>
     </table>
   <br/>
<script>
  var data;
  function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}

   $(document).ready(function(){
   	// code to read selected table row cell data (values).
   	$(".btnSelect").on('click',function(){
   		 var currentRow=$(this).closest("tr");

   		 var col2=currentRow.find("td:eq(1)").html();
   		 data=col2;
       window.location.href = "carrello.php?elimina=true&prodotto="+data.trim();
   	});
   });

</script>

<label id="prezzoFinale"><b>Prezzo totale dell'ordine: € </b><?php $trimmed = trim($costoTotale, " \0.");;echo $trimmed ?> </label>
<br/><br/>

<section id="second">
   <aside id="uno">
     <form >
     <br/>
<input type="checkbox" id="dom" name="domicilio" value="0"/><label id="domicilio" for="dom"> Consegna a domicilio </label>


<br/><br/>
       <label id="labelConsegna" for="con"> Indirizzo di consegna </label>
      <br/>
      <input type="text" id="con" name="consegna" />
    <br/><br/>
    <label id="labelData" for="dataConsegna"> Data </label>
  <br/>
  <input type="date" id="dataConsegna"name="data" />
<br/><br/>
<label id="labelOra" for="ora"> Ora consegna</label>
<br/>
<input type="time" id="ora"name="data" />






 </form>


</aside>
  <aside id="due">
    <form id="form2">

    <br/>
    <label id="labelCarta" for="carta"> Numero carta di credito </label>
  <br/>
   <input type="text" id="carta" name="carta" />
 <br/><br/>

 <label id="labelCodice" for="codice"> codice segreto</label>
<br/>
<input type="text" id="codice" name="codice" />
<br/><br/>
  <label for="mese"> Scadenza carta</label>
  <br/>
    <input type="text" id="mese" name="meseS" placeholder="mm"style="width:50px;"/>
    <label> / </label>
    <input type="text" id="anno" name="annoS" placeholder="yyyy" style="width:50px;"/>
    <label for="anno" style="visibility:hidden">/</label>
<br/><br/>
<input type="checkbox" id="ricorda" name="ricorda" value="1"/><label id="ric" for="ricorda"> Salva carta </label>


</form>

</aside>

</section>
<br/>
<input type="button" id="invia" value="Conferma ordine" name="ordina" onclick="effettuaOrdine()">
<?php
$utente = $_SESSION["utente"];
$getCard ="SELECT * FROM metodo_pagamento WHERE Username='$utente'";
$res = $conn->query($getCard);
if($res->num_rows > 0){

   $row = $res->fetch_assoc();
   ?><script>
   var numero = document.getElementById("carta");
   var codice = document.getElementById("codice");
   var meseScadenza = document.getElementById("mese");
   var annoScadenza = document.getElementById("anno");

   numero.value= <?php echo $row["Numero"] ?>;
   codice.value = <?php echo $row["Codice"] ?>;
   meseScadenza.value = <?php echo $row["Mese"] ?>;
   annoScadenza.value = <?php echo $row["Anno"] ?>;
   </script>
  <?php
}
?>
<script>
var indirizzo = document.getElementById("con");
var domicilio = document.getElementById("dom");
var ricorda = document.getElementById("ricorda");
var numero = document.getElementById("carta");
var codice = document.getElementById("codice");
var meseScadenza = document.getElementById("mese");
var annoScadenza = document.getElementById("anno");
var data = document.getElementById("dataConsegna");
var ora =  document.getElementById("ora");
//var time = data.getHours();
//var t= data.slice(0,10);
var g = document.getElementById("prezzoFinale");
var prezzo = "<?php echo $costoTotale ?>";

  domicilio.addEventListener('click', controlla);



function controlla(){
  //var g = document.getElementById("prezzoFinale");

  var string= "Prezzo totale dell'ordine: € ";
  var bold = string.bold();
  if(domicilio.checked){
    domicilio.value = 1;
    <?php $costoTotale = $costoTotale+0.70;?>
    g.innerHTML = bold+"<?php echo $costoTotale ?>";
    prezzo = "<?php echo $costoTotale ?>";
  }
  else{
    domicilio.value=0;
    <?php $costoTotale = $costoTotale-0.70;?>
    g.innerHTML = bold+"<?php echo $costoTotale ?>";
    prezzo = "<?php echo $costoTotale ?>";

  }

}
function effettuaOrdine(){
  var num = numero.value;
  var cod = codice.value;
  var ms = meseScadenza.value;
  var as = annoScadenza.value;
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();
  var hh = today.getHours();
  var min = today.getMinutes();
  var ore= ora.value.substr(0,2);
  var minuti = ora.value.substr(3,5);
  var anno = data.value.substr(0,4);
  var mese = data.value.substr(5,2);
  var giorno = data.value.substr(8,2);

  if(mm < 12 || meseScadenza < 12){
    mm = "0"+mm;
    ms="0"+ms;

  }
  if(prezzo == 0){
    alert("Nessun prodotto nel carrello, non è possibile ordinare");
  }
  else if((ore < 11 || ore > 21  || (ore == 21 && minuti > 00) ) && mese == mm && anno == yyyy){
    alert("Siamo disponibili solo dalle 11 alle 21");
    ora.value="11:00";
  }else if(ore < hh && giorno == dd ||(ore==hh && minuti < min && giorno == dd)  && mese == mm && anno == yyyy){
    alert("Ora di consegna errata,prossimo orario disponibile selezionato");
    //ora.value="11:00";
    ora.value=hh+":"+(min+30);
  }
  else if(giorno < dd || mese > mm || anno < yyyy || anno > yyyy){
    alert("Errore nella data selezionata, cambiala");
    data.value=yyyy+"-"+mm+"-"+dd;
    if(ore < 11 || ore > 21 || ore < hh || minuti < min ){
      ora.value="11:00";
    }
  }
  else if(domicilio.checked && indirizzo.value == "" ){
    alert("Seleziona il luogo di consegna");
  }else if(cod == "" ||  num.length != 16 || cod == "" || cod.length != 3 ){

      alert("Dati della carta errati, ricontrolla");

  }else if(ms < 1 || ms > 12 || ms < mm || as < yyyy){
    alert("Data di scadenza non valida");
  }
  else{
    if(ricorda.checked){
      window.location.href = "processaOrdine.php?data="+data.value+"&ora="+ora.value+"&indirizzo="+indirizzo.value+"&prezzo="+prezzo+"&domicilio="+domicilio.value+"&carta="+num+"&cod="+cod+"&ms="+ms+"&as="+as;
    }else{
      window.location.href = "processaOrdine.php?data="+data.value+"&ora="+ora.value+"&indirizzo="+indirizzo.value+"&prezzo="+prezzo+"&domicilio="+domicilio.value;
    }
  }
}
</script>

     <footer>

       <p id="footerPar"> 80voglia di piada s.r.l - Copyright 2018 - Email: <a id="mailLink" href="mailto:80vogliadipiada80@gmail.com">
  80vogliadipiada80@gmail.com</a></p>

     </footer>
</section>
   </body>

 </html>
