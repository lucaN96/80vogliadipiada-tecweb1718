<?php
session_start();
$servername="localhost";
$username ="root";
$password ="";
$database = "progetto";
$conn = new mysqli($servername,$username,$password,$database);
if(isset($_GET["elimina"])){

      $nomeP = $_GET["prodotto"];
      $select= "SELECT ID FROM prodotto WHERE Nome='$nomeP'";
      $res = $conn->query($select);
      $row = $res->fetch_assoc();
      $id= $row["ID"];
      $deleteQuery ="DELETE FROM prodotto WHERE ID='$id'";
      $res = $conn->query($deleteQuery);
}
if(isset($_GET["cat"])){

  $_SESSION["categoriaInserimento"] = $_GET["cat"];
}
else if(isset($_POST["aggiungiCategoria"]) && $_POST["cat"]!= "" ){
  $categoria = $_POST["cat"];
  $insertCat = "INSERT INTO categoria_prodotto (Nome) VALUES ('$categoria')";
  $res = $conn->query($insertCat);

}
else if(isset($_POST["aggiungiProdotto"]) && $_POST["prodotto"]!= "" && $_POST["prezzo"]!= "" && isset($_SESSION["categoriaInserimento"])){
  $nome = $_POST["prodotto"];
  $prezzo = $_POST["prezzo"];
  $cat = $_SESSION["categoriaInserimento"];
  $getID = "SELECT MAX(ID) AS ID FROM prodotto";
  $res = $conn->query($getID);
  $row = $res->fetch_assoc();
  $maxID = $row["ID"]+1;
  $insertProd = "INSERT INTO prodotto (ID,Nome,Prezzo,Categoria) VALUES ('$maxID','$nome','$prezzo','$cat')";
  $selectProd ="SELECT Nome,Prezzo FROM prodotto WHERE Nome='$nome'";
  $res = $conn->query($selectProd);

  if($res->num_rows == 0){ //prodotto non ancora presente
    $res = $conn->query($insertProd);
    unset($_SESSION["categoriaInserimento"]);
  }else{ //prodotto già presente
    $row=$res->fetch_assoc();
    $p = $row["Prezzo"];
    if($p != $prezzo){
      $update ="UPDATE prodotto SET Prezzo='$prezzo' WHERE Nome='$nome'";
      $res = $conn->query($update);
    }
  }

}
 ?>


<!DOCTYPE html>
<html lang="it">
  <head>
    <title> Admin</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css"  href="stileAdmin.css">
    <script src="jquery-3.2.1.min.js"></script>
  </head>
  <body >
<section>
    <header>

      <div id="loggedDiv">
          <a id="ordini" href="homePageAdmin.php"> Gestione ordini clienti </a>
        <figure>
            <img src="immagini/utente.png" alt="lucchetto" id="userLogo">
        </figure>

      <label id="user"> <?php echo $_SESSION["utente"] ?> </label>
      <a href="logout.php" id="logout"> Logout </a>

      </div>
      <figure>
          <img src="immagini/logo.png" alt="logo azienda" id="logo">
      </figure>

    </header>



    <form action="tariffario.php" method="post">
      <fieldset>

        <legend> Aggiunta categoria prodotti </legend>
        <br/>
        <label for="cat"> Aggiungi categoria di prodotti </label><br/>
          <input type="text" id="cat" name="cat"/>
        <input type="submit" name="aggiungiCategoria"  value="Aggiungi"/>
          </fieldset>
      </form>
    <br/>
      <form action="tariffario.php" method="post">
        <fieldset>

          <legend> Aggiunta prodotto </legend>
          <br/>
          <label id="info"> <b>Se il prodotto è già presente, il prezzo sarà aggiornato</b> </label>
        <br/><br/>
          <label for="categoria"> Scegli la categoria </label><br/>
          <select id="categoria">
            <?php
            if(!isset($_GET["cat"])){


             ?>
            <option> Scegli </option>
            <?php

            $query = "SELECT Nome FROM categoria_prodotto";
            $res = $conn->query($query);

            while($row=$res->fetch_assoc()){
              ?>
              <option value="<?php echo $row["Nome"] ?>"> <?php echo $row["Nome"] ?> </option>

              <?php
            }
          }else{

            ?>
           <option> <?php echo $_GET["cat"]?> </option>
           <?php
           $query = "SELECT Nome FROM categoria_prodotto WHERE  Nome NOT LIKE '".$_GET["cat"]."'";
           $res = $conn->query($query);

           while($row=$res->fetch_assoc()){
             ?>
             <option value="<?php echo $row["Nome"] ?>"> <?php echo $row["Nome"] ?> </option>

             <?php
           }

          }
            ?>
          </select><br/><br/>
          <label for="prodotto"> Nome prodotto </label><br/>
          <input type="text" id="prodotto" name="prodotto"/><br/><br/>
          <label for="prezzo"> Prezzo </label><br/>
          <input type="text" id="prezzo" name="prezzo"/><br/><br/>

          <input type="submit" id="invia" name="aggiungiProdotto" />
          <label for="invia" style="visibility:hidden;" >Bottone</label>

        <br/><br/>
          <label for="tabellaProdotti"> Elenco prodotti per categoria </label> <br/>
          <table class="table" id="tabellaProdotti" border="1" width="80%">
            <thead>
            <tr>
              <th id="n">Nome</th>
              <th id="p">Prezzo</th>
              <th id="e">Elimina</th>
            </tr>
            </thead>
            <tbody>
              <?php
              if(isset($_GET["cat"])){
                  $c = $_GET["cat"];
                  $cat = "SELECT Nome,Prezzo FROM prodotto WHERE Categoria='".$c."'";
                  $res = $conn->query($cat);
                  while($row=$res->fetch_assoc()){
                   ?>
                   <tr>
                     <td headers="n">
                       <?php




                        echo $row["Nome"]; ?>
                      </td>
                      <td headers="p">
                       <?php
                        echo $row["Prezzo"];

                         ?>
                      </td>
                      <td id="el" headers="e">
                       <!--<a id="elimina" href="elimina.php"> Elimina prodotto </a>-->
                       <input type="button"  class="btnSelect" value="Elimina"/>
                      </td>

                    </tr>
                    <?php
                  }

              }

     ?>


            </tbody>
          </table>
            </fieldset>
        </form>
      <script>

      var a= document.getElementById("categoria");
        function sceltaCategoria(){
            var str = a.options[a.selectedIndex].text;
            window.location.href = "tariffario.php?cat="+str;
        };
        a.addEventListener('change',sceltaCategoria,false);
      </script>
      <script>
      var data;
      function myTrim(x) {
        return x.replace(/^\s+|\s+$/gm,'');
    };

       $(document).ready(function(){
        // code to read selected table row cell data (values).
        $(".btnSelect").on('click',function(){
           var currentRow=$(this).closest("tr");

           var col2=currentRow.find("td:eq(0)").html();
           data=col2;

           window.location.href = "tariffario.php?elimina=true&prodotto="+data.trim()+"&cat=<?php echo $_GET["cat"]?>";
        });
       });

      </script>

    <footer>
      <p id="footerPar"> 80voglia di piada s.r.l - Copyright 2018 - Email: <a id="mailLink" href="mailto:80vogliadipiada80@gmail.com">
 80vogliadipiada80@gmail.com</a></p>

    </footer>
<script>

    var ordine = document.getElementById("ordine");
    function ottieniOrdine(){





            window.location.href="homePageAdmin.php?valore="+ordine.options[ordine.selectedIndex].text;

    }
    ordine.addEventListener('change',ottieniOrdine, false);

    </script>
</section>
  </body>
</html>
