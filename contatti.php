<!DOCTYPE html>
<html lang="it">
  <head>
    <title> 80voglia di piada</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css"  href="stileHomePage.css">
    <?php
    session_start();
    if(isset($_SESSION["utente"])){

    ?>
    <style>
      #loggedDiv{
        visibility: visible;
      }
      #normalDiv{
        visibility: hidden;
      }
    </style>
<?php
    }

     ?>

  </head>
  <body>
<section>
    <header>
      <div id="normalDiv">
        <figure>
            <img src="immagini/lucchetto.png" alt="lucchetto">
        </figure>
        <a href="login.html" id="login"> Login </a>
      </div>
      <div id="loggedDiv">
        <a href="ordinaProdotto.php" id="ordina"> Effettua un ordine </a>
        <a href="carrello.php" id="carrello"> Carrello </a>
        <label id="user"> <?php echo $_SESSION["utente"] ?> </label>
        <a href="logout.php" id="logout"> Logout </a>
      </div>
      <figure>
          <img id="logo" src="immagini/logo.png" alt="logo azienda">
      </figure>

    </header>
  <nav>
    <ul >
      <a href="homePage.php" id="home">Chi siamo </a>
       <a href="prodotti.php">Prodotti</a>
      <a href="contatti.php">Contattaci </a>
    </ul>
  </nav>
    <section id="listaC">
      <h1> I nostri contatti.</h1>
      <p>Telefono: 0547-84752.</p>
      <p> Indirizzo: Via Cervese 1084 Cesena, FC 47521. </p>
      <p> Email: <a id="mail" href="mailto:80vogliadipiada80@gmail.com"> 80vogliadipiada80@gmail.com.</a></p>
      <p> Aperti dal Lunedì al Venerdì, orario continuato dalle 11 alle 21. </p>

    </section>
    <footer>
      <p id="footerPar"> 80voglia di piada s.r.l - Copyright 2018 - Email: <a id="mailLink" href="mailto:80vogliadipiada80@gmail.com">
 80vogliadipiada80@gmail.com</a></p>

    </footer>
</section>
  </body>
















</html>
